import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys
import os
#import seaborn


###### Version and Date
prog_version = '1.0.0'
prog_date = '2016-07-13'

# Usage
usage = '''
==============================================================
           Version %s   wangtao3   %s

                    DongYang is Foolish
  
      Usage: python  <PyName>  <Input.txt>  <Output.png> 

   You can choose Output_file's extension ! eg. png,tif,jpg
==============================================================
'''%(prog_version, prog_date)


def getdatafromtxt(Input_file):
    data = {'A': [], 'C': [], 'G': [], 'T': []}
    with open(Input_file, 'r') as fhIn:
        for line in fhIn:
            line = line.split()
            data['A'].append(float(line[1]))
            data['C'].append(float(line[2]))
            data['G'].append(float(line[3]))
            data['T'].append(float(line[4]))
    return data


def drawhistogram(data, Output_file):
    fig, axes = plt.subplots(nrows=4, ncols=1)

    for i, key in zip(range(4), sorted(data.keys())):
        axes[i].hist(np.array(data[key]), bins=500, range=(-0.2,1.0), normed=1, facecolor='green', alpha=0.5)
        axes[i].set_xlabel("Intensity of %s" % key)
        axes[i].set_ylabel("Frequency")

    fig.subplots_adjust(wspace=0.6, hspace=0.6)
    fig.savefig(Output_file, dpi=500)

#################################
##
# Main function of program.
##
#################################


def Main():

    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage=usage, version=prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (Input_file, Output_file) = args

    ############################# Main Body #############################
    a = getdatafromtxt(Input_file)
    drawhistogram(a, Output_file)

#################################
##
# Start the main program.
##
#################################
if __name__ == '__main__':
    Main()
